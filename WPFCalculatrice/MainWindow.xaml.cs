﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFCalculatrice
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Zero_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "0";
            Reponse.Text += "0";
        }

        private void Un_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "1";
            Reponse.Text += "1";
        }

        private void Deux_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "2";
            Reponse.Text += "2";
        }

        private void Trois_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "3";
            Reponse.Text += "3";
        }

        private void Quatre_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "4";
            Reponse.Text += "4";
        }

        private void Cinq_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "5";
            Reponse.Text += "5";
        }

        private void Six_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "6";
            Reponse.Text += "6";
        }

        private void Sept_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "7";
            Reponse.Text += "7";
        }

        private void Huit_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "8";
            Reponse.Text += "8";
        }

        private void Neuf_Click(object sender, RoutedEventArgs e)
        {
            //this.Nombre[this.indexList] = this.Nombre[indexList] + "9";
            Reponse.Text += "9";
        }

        private void Virgule_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text += ",";
        }

        private void AC_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text = null;
        }

        private void Negatif_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text += "-";
        }

        private void Diviser_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text += "/";
        }

        private void Multiplier_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text += "*";
        }

        private void Soustraire_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text += "-";
        }

        private void Additionner_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text += "+";
        }

        private void DiviserPar100_Click(object sender, RoutedEventArgs e)
        {
            Reponse.Text += "/100";
        }

        private void Egale_Click(object sender, RoutedEventArgs e)
        {
            /*
            string[] ops = Regex.Split(Reponse.Text, @"[0-9,]+");
            List<string> opList = ops.ToList();
            opList.RemoveAll("");
            string[] valeurString = Regex.Split(Reponse.Text, @"\+\-\*\/");
            */
            string Result = Reponse.Text.Replace(",", ".");
            DataTable dt = new DataTable();
            object resultat = dt.Compute(Result,"");

            string Res = resultat.ToString();
            Reponse.Text = Res.Replace(".",",");

        }

    }
}
