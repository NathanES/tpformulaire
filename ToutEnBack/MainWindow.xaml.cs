﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ToutEnBack
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LinearGradientBrush linGrBrush = new LinearGradientBrush(Color.FromArgb(255, 255, 100, 70), Color.FromArgb(255, 50, 90, 255),50);
            Grid GridGlobal = new Grid()
            {
                //ShowGridLines = true,
                Height = 450,
                Width = 800,
                Background = linGrBrush
            };

            for(int i = 0; i < 6 ; i++)
            {
                RowDefinition rowDef = new RowDefinition();
                GridGlobal.RowDefinitions.Add(rowDef);
            }
            for(int i = 0; i < 4; i++)
            {
                ColumnDefinition colDef = new ColumnDefinition();
                GridGlobal.ColumnDefinitions.Add(colDef);
            }
            Content = GridGlobal;
        }
    }
}
