﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFFormulaire.Modele;

namespace WPFFormulaire
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        bool NomVrai = true;
        bool PrenomVrai = true;
        bool AgeVrai = true;

        Personne lambda = new Personne("", "", 0, "");
        static int index = 0;
        static List<Personne> Listp = new List<Personne>();
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            Age.MaxLength = 3;
            Regex TNomPrenom = new Regex(@"^[A-Z]?[a-zA-Z]+$");
            Regex TAge = new Regex(@"^[0-9]+$");

            NomVrai = TNomPrenom.IsMatch(Nom.Text);
            PrenomVrai = TNomPrenom.IsMatch(Prenom.Text);
            AgeVrai = TAge.IsMatch(Age.Text);


            if(!NomVrai)
            {
                Nom.Text = "Veuillez resaisir votre Nom";
            }
            if(!PrenomVrai)
            {
                Prenom.Text = "Veuillez resaisir votre Prenom";
            }
            if(!AgeVrai)
            {
                Age.Text = "Veuillez resaisir votre Age";
            }
            if(AgeVrai&&PrenomVrai&&NomVrai)
            {
                Remplissage();
                Efface();
                Nom.Text = "Ajout dans la liste";
            }
        }
        public void Remplissage()
        {
            lambda.Nom = Nom.Text;
            lambda.Prenom = Prenom.Text;
            lambda.Age = Int32.Parse(Age.Text);
            lambda.Description = Description.Text;
            Listp.Add(lambda);
            index++;
        }
        public void Efface()
        {
            Nom.Text = "";
            Prenom.Text = "";
            Age.Text = "";
            Description.Text = "";
        }
    }
}
