﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFFormulaire.Modele
{
    public class Personne
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }
        public string Description { get; set; }
        public int ID { get; set; }
        public static int IDGlobal = 0;
        public Personne(string nom, string prenom, int age, string description)
        {
            this.ID = IDGlobal;
            this.Nom = nom;
            this.Prenom = prenom;
            this.Age = age;
            this.Description= description;
            IDGlobal++;
        }
    }
}
