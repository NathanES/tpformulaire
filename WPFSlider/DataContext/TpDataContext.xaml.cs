﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFSlider.Models;

namespace WPFSlider.DataContext
{
    /// <summary>
    /// Logique d'interaction pour TpDataContext.xaml
    /// </summary>
    public partial class TpDataContext : Window
    {

        Personne t = new Personne(10, "test", "osez");
        Personne p = new Personne(110, "teqsdst", "oqsdsez");
        public TpDataContext()
        {
            InitializeComponent();
            DataContext = t;
            DataContext = p;
        }

        private void Window_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }
    }
}
