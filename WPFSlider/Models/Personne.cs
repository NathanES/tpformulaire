﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFSlider.Models
{
    class Personne
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public Personne(int id,string nom,string prenom)
        {
            this.Id = id;
            this.Nom = nom;
            this.Prenom = prenom;
        }
        public Personne()
        {

        }
    }
}
